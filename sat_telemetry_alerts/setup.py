from setuptools import setup

setup(
    name='sat_telemetry_alerts',
    version='1.1.0',
    packages=[''],
    package_dir={'': '.idea/inspectionProfiles'},
    url='https://github.com/zachiefish/sat_telemetry_alerts',
    license='MIT',
    author='Chris Magee',
    author_email='crmagee89@gmail.com',
    description='Paging Mission Control Satellite Data',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.10",
)
